<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Учебная практика 2021</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link rel="stylesheet" type="text/css" href="index.css" >
</head>
<body>
	<h1>Производственная практика 2021</h1>
	<div id="btn_bkup">
		<form method="post">
			<button name="export">Сделать бекап</button>
			<button name="import">Выбрать бекап</button>
			<input type="text" name="backup_name">
		</form>
	</div>

	<div id="adding_new_horse">
		<table>
			<tr>
				<td>ID</td>
				<td>Дата рождения</td>
				<td>Порода</td>
				<td>Цвет</td>
				<td>Пол</td>
				<td>Прозвище</td>
				<td>ID владельца</td>
			</tr>
			<tr>
				<form method="post">
					<td><input type="text" name="id"></td>
					<td><input type="text" name="birth_date"></td>
					<td><input type="text" name="breed"></td>
					<td><input type="text" name="color"></td>
					<td><input type="text" name="gender"></td>
					<td><input type="text" name="nickname"></td>
					<td><input type="text" name="owner_id"></td>
					<td><input type="submit" name="add" value="Добавить нового пользователя"></td>
				</form>
			</tr>
		</table>
	</div>

	<div id="filters">
		<table>
			<tr>
				<td>ID</td>
				<td>Дата рождения</td>
				<td>Порода</td>
				<td>Цвет</td>
				<td>Пол</td>
				<td>Прозвище</td>
				<td>ID владельца</td>
			</tr>
			<tr>
				<form method="post">
					<td><input type="text" name="id"></td>
					<td><input type="text" name="birth_date"></td>
					<td><input type="text" name="breed"></td>
					<td><input type="text" name="color"></td>
					<td><input type="text" name="gender"></td>
					<td><input type="text" name="nickname"></td>
					<td><input type="text" name="owner_id"></td>
					<td><input type="submit" name="filter" value="Отфильтровать таблицу"></td>
					<td><input type="submit" name="unfilter" value="Показать все записи"></td>
				</form>
			</tr>
		</table>
	</div>
	<div>
		<table id="table_horse" border="1">
			<tr>
				<td>ID</td>
				<td>Дата рождения</td>
				<td>Порода</td>
				<td>Цвет</td>
				<td>Пол</td>
				<td>Прозвище</td>
				<td>ID владельца</td>
			</tr>
<?php
include 'db_connect.php';
include 'fill_table.php';

if (array_key_exists("unfilter", $_POST)) {
	FillTable(OpenConnection());
}

if (array_key_exists("filter", $_POST)) {

    $id = $_POST['id'];
    $birth_date = $_POST['birth_date'];
    $breed = $_POST['breed'];
    $color = $_POST['color'];
    $gender = $_POST['gender'];
    $nickname = $_POST['nickname'];
    $owner_id = $_POST['owner_id'];

    $connection = OpenConnection();

    $query = "SELECT * FROM `horses` WHERE `id` = '$id' or `birth_date` = '$birth_date' or `breed` = '$breed' or `color` = '$color' or `gender` = '$gender' or `nickname` = '$nickname' or `owner_id` = '$owner_id'";

    $result = mysqli_query($connection, $query) or die("Ошибка " . mysqli_error($link));

    while ($row = mysqli_fetch_row($result)) {
        echo "<tr>";
        for ($j = 0;$j < 7;++$j) {
            echo "<td>$row[$j] </td>";
        }
        echo "</tr>";
    }
    CloseConnection($connection);
}

if (array_key_exists("add", $_POST)) {

	$id = $_POST['id'];
    $birth_date = $_POST['birth_date'];
    $breed = $_POST['breed'];
    $color = $_POST['color'];
    $gender = $_POST['gender'];
    $nickname = $_POST['nickname'];
    $owner_id = $_POST['owner_id'];

    $connection = OpenConnection();

    $query = "INSERT INTO `horses` (`id`, `birth_date`, `breed`, `color`, `gender`, `nickname`, `owner_id`) VALUES ('$id', '$birth_date', '$breed', '$color', '$gender', '$nickname', '$owner_id') ";

    $result = mysqli_query($connection, $query) or die(mysqli_error($link)."".FillTable($connection)); 
    FillTable($connection);
    CloseConnection($connection);
}

if (array_key_exists("export", $_POST)) {
    include "export.php";
    backup_tables('localhost', 'root', 'root', 'horses');
    FillTable(OpenConnection());
}

if (array_key_exists("import", $_POST)) {
	include "import.php";
	$backup_name = $_POST['backup_name'];
	import_table("backup/".$backup_name);
	FillTable(OpenConnection());
}
?>
		</table>
	</div>
	<div>
	</div>
</body>
</html>