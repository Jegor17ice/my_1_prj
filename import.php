<?php
function import_table($file_name) {

	// MySQL host
	$mysql_host = 'localhost';
	// MySQL username
	$mysql_username = 'root';
	// MySQL password
	$mysql_password = 'root';
	// Database name
	$mysql_database = 'horses';

	// Connect to MySQL server
	$link = mysqli_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysqli_error());
	// Select database
	mysqli_select_db($link, $mysql_database) or die('Error selecting MySQL database: ' . mysqli_error());

	// Temporary variable, used to store current query
	$templine = '';
	// Read in entire file
	$lines = file($file_name);
	// Loop through each line
	foreach ($lines as $line)
	{
	// Skip it if it's a comment
	if (substr($line, 0, 2) == '--' || $line == '')
	    continue;

	// Add this line to the current segment
	$templine .= $line;
	// If it has a semicolon at the end, it's the end of the query
	if (substr(trim($line), -1, 1) == ';')
	{
	    // Perform the query
	    mysqli_query($link, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error() . '<br /><br />');
	    // Reset temp variable to empty
	    $templine = '';
	}
	}
}
?>