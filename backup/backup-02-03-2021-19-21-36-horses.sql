SET foreign_key_checks = 0;

DROP TABLE cups;

CREATE TABLE `cups` (
  `ID` int(11) NOT NULL,
  `id_horse` int(11) NOT NULL,
  `id_jockey` int(11) NOT NULL,
  `id_owner` int(11) NOT NULL,
  `id_stadium` int(11) NOT NULL,
  `rase_num` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXFK_cups_horses` (`ID`),
  KEY `IXFK_cups_jockeys` (`ID`),
  CONSTRAINT `FK_cups_horses` FOREIGN KEY (`ID`) REFERENCES `horses` (`id`),
  CONSTRAINT `FK_cups_jockeys` FOREIGN KEY (`ID`) REFERENCES `jockeys` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE horses;

CREATE TABLE `horses` (
  `id` int(11) NOT NULL,
  `birth_date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO horses VALUES("1","13-01-05","Арабская лошадь","Серый","Ж","Лунка","1");
INSERT INTO horses VALUES("2","03-02-01","Карачаевская ","Белый","М","-","2");
INSERT INTO horses VALUES("3","02-01-93","Карачаевская ","Черный","Ж","-","3");
INSERT INTO horses VALUES("4","29-07-10","Арабская лошадь","Серый","М","Красавиц","4");
INSERT INTO horses VALUES("5","19-08-15","Англо-норманн","Коричневый","М","Англичанин","5");



DROP TABLE jockeys;

CREATE TABLE `jockeys` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` float DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE owners;

CREATE TABLE `owners` (
  `id` int(11) NOT NULL,
  `adress` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE stadiums;

CREATE TABLE `stadiums` (
  `id` int(11) NOT NULL,
  `adress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `road_length` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE users;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




SET foreign_key_checks = 1;